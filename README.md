# Snatch (porcs i diamants)
## Any producció
2000

## Argument 
 El destí creuat de diversos personatges, entre els quals Franky anomenat "Franky quatre dits" que ha de lliurar al "Cosí Avi", un mafiós novaiorquès, un enorme diamant que acaba de robar.

## Actors
- Jason Statham
- Brad Pitt
- Benicio Del Toro

## Urls
[IMBD](https://www.imdb.com/title/tt0208092/)

## Java

```java 
public static void SayName(){
    System.out.println("Snatch (porcs i diamants)");
}
```